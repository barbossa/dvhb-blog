from django.conf.urls import include, url
from django.contrib import admin
from blog.views import index, login, add_post, follow, unfollow, feed, view_post, logout
from django.contrib.auth import views as auth_views


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^id/(?P<pk>[0-9]+)/', index, name='index'),
    url(r'^add-post/', add_post, name='add-post'),
    url(r'^post/(?P<pk>[0-9]+)/', view_post, name='view-post'),
    url(r'^user/(?P<pk>[0-9]+)/follow$', follow, name='follow'),
    url(r'^user/(?P<pk>[0-9]+)/unfollow$', unfollow, name='unfollow'),
    url(r'^feed/', feed, name='feed'),
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
    url(r'^admin/', include(admin.site.urls)),
]