from django import forms


class AddPost(forms.Form):
    title = forms.CharField(label='Title', required=True, max_length=250,
                            widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Title'}))
    description = forms.CharField(label='Description', required=True,
                                  widget=forms.Textarea(attrs={'class': 'form-control', 'placeholder': 'Description'}))


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control', 'placeholder': 'Password'}))