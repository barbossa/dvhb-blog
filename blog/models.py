from django.db import models
from datetime import datetime
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.core.mail import send_mail
from django.contrib.sites.models import Site


class PersonalBlog(models.Model):
    user = models.ForeignKey(User)
    title = models.CharField(max_length=250, default='May the Force be with you')
    created_date = models.DateTimeField(default=datetime.now)

    def __unicode__(self):
        return self.user.username


def user_post_save(sender, instance, created, **kwargs):
    """Create a user blog when a new user account is created"""
    if created is True:
        blog = PersonalBlog()
        blog.user = instance
        blog.save()


post_save.connect(user_post_save, sender=User)


class Post(models.Model):
    blog = models.ForeignKey(PersonalBlog)
    title = models.CharField(max_length=250)
    description = models.TextField()
    created_date = models.DateTimeField(default=datetime.now)

    def get_link(self):
        return '/post/{0}'.format(self.id)

    def get_absolute_url(self):
        """For emails notification"""
        domain = Site.objects.get_current()
        return '{0}post/{1}'.format(domain, self.id)

    def __unicode__(self):
        return self.title


def post_post_save(sender, instance, created, **kwargs):
    """Notify all followers when new post added"""
    if created is True:
        user = instance.blog.user
        followers = Follow.objects.filter(followee=user)
        for follower in followers:
            subject = '{0} added new post'.format(user.username)
            message = 'Hi, {0}! {1} added a new post <a href="{2}">{3}</a>'.format(follower.follower.username,
                                                                                   user.username,
                                                                                   instance.get_absolute_url(), instance.title)
            print subject, message, follower.follower.email
            send_mail(subject, message, 'noreplay@userblog.com', follower.follower.email, fail_silently=False)


post_save.connect(post_post_save, sender=Post)


class Follow(models.Model):
    followee = models.ForeignKey(User, related_name='followers')
    follower = models.ForeignKey(User, related_name='followees')