from django.contrib import admin
from blog.models import PersonalBlog, Post, Follow


class PersonalBlogAdmin(admin.ModelAdmin):
    list_display = ['user', 'title', 'created_date']


class PostAdmin(admin.ModelAdmin):
    list_display = ['blog', 'title', 'description', 'created_date']

class FollowAdmin(admin.ModelAdmin):
    list_display = ['followee', 'follower']


admin.site.register(PersonalBlog, PersonalBlogAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(Follow, FollowAdmin)