from blog.models import PersonalBlog, Post
from django.contrib.auth.models import User
from django.test import TestCase


class BlogTests(TestCase):
    def test_create_blog_when_user_created(self):
        self.user = User.objects.create_user(username='john', email='jlennon@beatles.com', password='1')
        self.blog = PersonalBlog.objects.get(user=self.user)
        self.assertEqual(self.user.username, self.blog.user.username)


# class PostTests(TestCase):
#     def test_user_can_create_new_post(self):
#         self.user = User.objects.create_user(username='john', email='jlennon@beatles.com', password='1')
#         self.blog = PersonalBlog.objects.create(user=self.user, title='test title')
#         self.post = Post.objects.create()
