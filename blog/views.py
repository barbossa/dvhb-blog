from django.shortcuts import get_object_or_404, render
from blog.models import PersonalBlog, Post, Follow
from django.shortcuts import redirect
from blog.forms import AddPost, LoginForm
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout


@login_required
def index(request, pk=None):
    user = request.user
    is_follow = False
    if pk:
        user = User.objects.get(id=pk)
        is_follow = Follow.objects.filter(follower=request.user, followee=user).exists()
    posts = Post.objects.filter(blog__user=user)
    users = User.objects.all()
    followees = Follow.objects.filter(follower=user)
    return render(request, 'index.html',
                  {'user': user, 'posts': posts, 'users': users, 'followees': followees, 'pk': pk,
                   'is_follow': is_follow})


@login_required
def add_post(request):
    user = request.user
    if request.method == 'POST':
        form = AddPost(request.POST)
        if form.is_valid():
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            blog = PersonalBlog.objects.get(user=user)
            Post.objects.create(blog=blog, title=title, description=description)
            return HttpResponseRedirect('/')

    else:
        form = AddPost()

    return render(request, 'add_post.html', {'form': form})


def view_post(request, pk):
    post = get_object_or_404(Post, id=pk)
    # post = Post.objects.get(id=pk)
    return render(request, 'view_post.html', {'post': post})


@login_required
def feed(request):
    user = request.user
    followees_ids = Follow.objects.values_list('followee__id', flat=True).filter(follower=user)
    posts = Post.objects.filter(blog__user__id__in=set(followees_ids)).order_by('created_date')
    return render(request, 'feed.html', {'posts': posts})


@login_required
def follow(request, pk):
    user = get_object_or_404(User, id=pk)
    Follow.objects.get_or_create(followee=user, follower=request.user)
    return redirect('/feed')


@login_required
def unfollow(request, pk):
    user = get_object_or_404(User, id=pk)
    Follow.objects.filter(followee=user, follower=request.user).delete()
    return redirect('/feed')


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                auth_login(request, user)
                return HttpResponseRedirect('/')
            else:
                return HttpResponseRedirect('/login')
    else:
        form = LoginForm()
    return render(request, 'login.html', {'form': form})


def logout(request):
    auth_logout(request)
    return HttpResponseRedirect('/login')