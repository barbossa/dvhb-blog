# Установка #

Если необходимо, установите недостающие пакеты 
```
#!bash
sudo apt-get install python3 libpq-dev python-dev postgresql postgresql-contrib
```
## Разворачивание проекта ##

**Вариант 1**. С новой базой
```
#!bash
sudo su - postgres
createdb dvhb
exit
sudo service postgresql restart

git clone git@bitbucket.org:barbossa/dvhb-blog.git
cd dvhb-blog
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
python manage.py runserver 0.0.0.0:8000
```
**Вариант 2**. С моей базой. Я рекомендую этот вариант, так как я её уже наполнил тестовыми данными (постами, юзерами).
```
#!bash
git clone git@bitbucket.org:barbossa/dvhb-blog.git
cd dvhb-blog
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt

sudo su - postgres
createdb dvhb
psql dvhb < dvhb.sql
exit
sudo service postgresql restart

python manage.py runserver 0.0.0.0:8000
```

Админский пользователь:
username - dvhb
password - 1

У всех остальных пользователей пароль - 1